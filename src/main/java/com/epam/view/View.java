package com.epam.view;

import com.epam.controller.ControllerImplementation;
import com.epam.controller.IController;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View  {
    private IController controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public View() {
        controller = new ControllerImplementation();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Gson");
        menu.put("2", "  2 - Jackson");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);

    }
    private void pressButton1() {
        controller.parseGson();
    }
    private void pressButton2() {
        controller.parseJackson();
    }
    private void outputMenu() {
        System.out.println("\nMenu:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }
    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select an item.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

}
