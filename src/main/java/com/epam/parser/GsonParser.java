package com.epam.parser;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.epam.comparator.CandyComparator;
import com.epam.model.Candy;
public class GsonParser {
    private static Logger log = LogManager.getLogger(GsonParser.class);
    private CandyComparator comparator;

    public GsonParser() {
        comparator = new CandyComparator();
    }

    public List<Candy> parse(File jsonFile) {
        Type listType = new TypeToken<ArrayList<Candy>>() {
        }.getType();

        List<Candy> candies = null;
        try {
            candies = new Gson().fromJson(new FileReader(jsonFile), listType);
            candies.sort(comparator);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return candies;
    }
}
