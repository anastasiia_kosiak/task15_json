package com.epam.controller;

public interface IController {
    void parseGson();
    void parseJackson();
}
