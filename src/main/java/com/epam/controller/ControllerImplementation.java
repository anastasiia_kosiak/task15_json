package com.epam.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.epam.parser.GsonParser;
import com.epam.parser.JacksonParser;
import com.epam.parser.JsonValidate;
import java.io.File;

public class ControllerImplementation implements IController {
    private GsonParser gsonParaser;
    private JacksonParser jacksonParser;
    private JsonValidate jsonValidate;
    private File jsonSchemaPath;
    private File jsonPath;
    private static Logger log = LogManager.getLogger(ControllerImplementation.class);

    public ControllerImplementation() {

        jsonSchemaPath = new File("D:\\task15_json\\src\\main\\resources\\sweetSchema.json");
        jsonPath = new File("D:\\task15_json\\src\\main\\resources\\sweet.json");
        jsonValidate = new JsonValidate();
        gsonParaser = new GsonParser();
        jacksonParser = new JacksonParser();
    }

    public void parseJackson() {
        if (jsonValidate.isValid(jsonPath, jsonSchemaPath)) {
            jacksonParser.parse(jsonPath).forEach(log::info);
        } else {
            log.error("JSON is not valid");
        }
    }

    public void parseGson() {
        if (jsonValidate.isValid(jsonPath, jsonSchemaPath)) {
            gsonParaser.parse(jsonPath).forEach(log::info);
        } else {
            System.out.println("error in controller");
        }
    }

}
